ARG RUBY_VERSION=2.7-bullseye
FROM ruby:${RUBY_VERSION} as ruby

ENV RAILS_ENV=production \
  BUNDLE_JOBS=10 \
  BUNDLE_RETRY=3 \
  GEM_HOME=/bundle
ARG BUILD_DECIDIM_VERSION=0.24.3
ENV DECIDIM_VERSION=${BUILD_DECIDIM_VERSION}

ENV DEBIAN_RELEASE=bullseye

ENV NODE_KEYRING=/usr/share/keyrings/nodesource.gpg \
    NODE_VERSION=node_14.x

ENV SECRET_KEY_BASE=f97271c0788641d98a8a7feaa2b8b40fdc28f83285a4f23703abdaf3ac0641a4f047788fd15e4b698e026325ebda371573c370fd6a3bdb720d7e04a580b84882 \
    RAILS_SERVE_STATIC_FILES=true

RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource.gpg.key | gpg --dearmor | tee "$NODE_KEYRING" >/dev/null
RUN echo "deb [signed-by=$NODE_KEYRING] https://deb.nodesource.com/$NODE_VERSION $DEBIAN_RELEASE main" | tee /etc/apt/sources.list.d/nodesource.list
RUN echo "deb-src [signed-by=$NODE_KEYRING] https://deb.nodesource.com/$NODE_VERSION $DEBIAN_RELEASE main" | tee -a /etc/apt/sources.list.d/nodesource.list

RUN apt-get update && apt-get install -y libyaml-dev nodejs imagemagick yarn libicu-dev postgresql-client openssl nano bash curl git && apt-get autoremove && apt-get clean all

RUN gem install bundler bootsnap listen decidim:${DECIDIM_VERSION} railties
RUN mkdir -p -m 770 /decidim && $GEM_HOME/bin/decidim /decidim

WORKDIR /decidim

COPY Gemfile* .

RUN bundle install

COPY . .

RUN $GEM_HOME/bin/rails assets:precompile
RUN $GEM_HOME/bin/rails generate wicked_pdf

RUN chgrp -R 0 /decidim && chmod -R g=rwx /decidim

VOLUME /decidim/public/uploads
VOLUME /decidim/config
EXPOSE 3000

ENTRYPOINT ["/bundle/bin/rails"]
CMD ["s"]